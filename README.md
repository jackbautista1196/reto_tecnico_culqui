# reto_tenico

Este proyecto tiene como finalidad la busqueda de personajes de la reconocida serie Rick and Morty
Para levantar el proyecto en entorno local deberá ejecutar los siguientes pasos
## Instalacion de dependencias
npm install

## Ejecutar
vue serve

### Build de la aplicación
```
vue build
```
Para utilizar la aplicación solo deberá ingresar una palabra de búsqueda y seleccionar un estado que por default esta seleccionado la opción "alive", una vez realizado eso, se listará los resultados y podrá observar mas información haciendo Clic en cualquiera de los resultados.

